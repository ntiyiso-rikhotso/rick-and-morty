<?php
/**
 * @file
 * Contains Drupal\ricky_and_morty\Form\MessagesForm.
 */

namespace Drupal\ricky_and_morty\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigRickyAndMortyForm
 *
 * @package Drupal\ricky_and_morty\Form
 */
class ConfigRickyAndMortyForm extends ConfigFormBase
{
    /**
     * Current Form ID
     */
    private const FORM_ID = 'ricky_and_morty';
    /**
     * Table Name For Storing Data For The Current Form
     */
    public const TABLE_CONFIG_NAME = 'ricky_and_morty.admin_settings';
    /**
     * The Fields To Search By From The API
     */
    public const FIELD_SEARCH_API_BY = 'search_api_by';
    /**
     * Search Rick And Morty Input Placeholder Text
     */
    public const FIELD_PLACEHOLDER_NAME = 'placeholder';
    /** API url field   */
    public const FIELD_API_URL = 'api_url';

    private const ALLOWED_FILTER_KEYS = [
        'Name',
        'Status',
        'Species',
        'Type',
        'Gender',
    ];

    /**
     * @return string
     */

    public function getFormId(): string
    {
        return self::FORM_ID;
    }

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration() : array
    {
        $defaultConfig = \Drupal::config(self::TABLE_CONFIG_NAME);
        return [
            self::FIELD_PLACEHOLDER_NAME => $defaultConfig->get(self::FIELD_PLACEHOLDER_NAME),
            self::FIELD_SEARCH_API_BY => $defaultConfig->get(self::FIELD_SEARCH_API_BY),
            self::FIELD_API_URL => $defaultConfig->get(self::FIELD_API_URL),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state): array
    {
        $config = $this->config(self::TABLE_CONFIG_NAME);

        $form[self::FIELD_API_URL] = [
            '#type'     => 'textfield',
            '#title'    => t('API Url:'),
            '#required' => false,
            '#default_value' => $config->get(self::FIELD_SEARCH_API_BY) ?: 'https://rickandmortyapi.com/api/character'
        ];

        $form[self::FIELD_PLACEHOLDER_NAME] = [
            '#type'     => 'textfield',
            '#title'    => t('Placeholder Text'),
            '#required' => false,
        ];
        $allowed = implode(', ',self::ALLOWED_FILTER_KEYS);
        $fieldDescription = "Please enter fields and separate them by comma(,). Allowed ($allowed)";

        $form[self::FIELD_SEARCH_API_BY] = [
            '#type'          => 'textfield',
            '#title'         => t('Search API By:'),
            '#default_value' => $config->get(self::FIELD_SEARCH_API_BY) ?: 'Name',
            '#description'   => t($fieldDescription),
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state): void
    {
        parent::submitForm($form, $form_state);

        $config = $this->config(self::TABLE_CONFIG_NAME);

        // fields we need to save from config
        $fields = [
            self::FIELD_PLACEHOLDER_NAME,
            self::FIELD_API_URL
        ];
        foreach ($fields as $field) {
            $config->set($field, $form_state->getValue($field));
        }

        // search api by
        $value = explode(',', $form_state->getValue(self::FIELD_SEARCH_API_BY));

        // Allowed filter fields
        $allowed = self::ALLOWED_FILTER_KEYS;

        // filter fields
        $filtered = array_filter(
            $value,
            static function ($key) use ($allowed) {
                $key = ucfirst(trim($key));
                return in_array($key, $allowed);
            });

        $imploded = !$filtered ? 'Name' : implode(',', $filtered);
        // set field value, value is imploded e.g name,age
        $config->set(self::FIELD_SEARCH_API_BY, $imploded);

        $config->save();
    }

    protected function getEditableConfigNames(): array
    {
        return [
            self::TABLE_CONFIG_NAME,
        ];
    }
}
