<?php

/**
 * @file
 */
namespace Drupal\ricky_and_morty\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Creates a 'SearchRickAndMortyBlock' Block
 * @Block(
 * id = "block_search_ricky_and_morty",
 * admin_label = @Translation("Search Rick and Morty Block"),
 * )
 */
class SearchRickAndMortyBlock extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function build() {
        return [
            'search_box' => [
                '#theme'   => 'search_box',
                '#attached' => [
                    'library' => [
                        'ricky_and_morty/search',
                    ],
                ],
                '#cache' => [
                    'max-age' => 0
                ]
            ],
        ];
    }

}
