const app = new Vue({
  el: '#app',
  data(){
    return {
      input: '',
      error: null,
      result: null,
      // API settings
      config: drupalSettings.rickyAndMortyConfig || null,
      styles: {
        width: 'auto',
        height: 'auto',
        'min-width': '100px',
        'min-height': '100px',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center center',
        background: ''

      }
    }
  },
  methods: {
    /**
     * This submit search text to the API for search
     * @param text
     */
    submit({ text: searchString }){
      app.error = null
      const searchApiByKeys = (app.config ? app.config.searchApiBy : 'name').split(',')
      const params = {}
      searchApiByKeys.forEach(value => {
        let key = value.toLocaleLowerCase();
        params[key] = searchString
      })

      // serialize to get key-value as query string for security purpose
      const queryString = app.serialize(params)
      const url = `https://rickandmortyapi.com/api/character?${queryString}`

      axios.get(url).then(({ data }) => {
        const { results } = data
        // API always return an array so always try to get a random element as per request from project scope
        const rand = results.length * Math.random()
        app.result = results[ rand | 0 ]
      }).catch(({ response: {status} }) => {
        // Stores errors by HTTP code
        const errors = {
          // API return a 404 if there are no results
          404 : `No Data Found For ${searchString}`
        }
        app.result = null
        app.error = errors[parseInt(status)] || 'An Error Occurred'
      })
    },
    /**
     * Create query string from object
     * @param params
     * @return {string}
     */
    serialize(params) {
      const e = v => encodeURIComponent(v);
      return Object.entries(params).map(([key, value]) => {
        return [e(key), e(value)].join('=')
      }).join("&");
    }
  },
  // Template
  template: `
  <div class="container">
    <div  class="row">
  <div id="ricky-and-morty" class="rectangle-2 col-md-12">
    <div class="logo row pb-1">
        <div class="col text-center">
        <img class="rounded img-fluid" alt="Logo">
      </div>
    </div>
    <div class="input row">
      <input :placeholder="config ? config.placeholder : 'Enter a character’s name to get more information'" type="text" @keypress.enter="submit({ text: input })" v-model="input"/>
    </div>
    <div if="error">
      <div class="row">
      <div class="col text-danger text-center">
        <p>{{ error }}</p>
      </div>
      </div>
    </div>
    <div class="rectangle-4 row p-3 m-3" v-if="result">
      <div class="col">
      <div class="row justify-content-between">
          <div class="col-auto pp">
              <img :src="result.image" class="img-fluid rounded m-0 mb-1 mt-3" :alt="result.name">
          </div> 
           <div class="col pl-4 text-black">
            <table class="table borderless">
              <thead class="names">
                <tr>
                  <th class="text-bold pb-3" scope="col" colspan="2">{{ result.name }}</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Species</td>
                  <td>{{ result.species }}</td>
                </tr>
                <tr>
                  <td>Gender</td>
                  <td>{{ result.gender }}</td>
                </tr>
                <tr>
                  <td>Origin</td>
                  <td>{{ result.origin.name }}</td>
                </tr>
                <tr>
                  <td>Status</td>
                  <td>{{ result.status }}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
       </div>
    </div>
   </div>
    </div>
  </div>
`
});
